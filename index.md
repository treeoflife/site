---
layout: layout.liquid
pageTitle: 'About Us'
---

Café, bookshop, community hub, spiritual oasis, music venue…

…

Perhaps you’re just looking for a quiet cuppa in central Liverpool–,
or perhaps you’re looking for some edifying reading from our [historic](media/history.pdf) bookshop–
or maybe you’re called to join the team of volunteers who make it all possible.

Maybe the best way to find out more is to pop in and say a quick Hello!
You can find us on 18 Slater Street, L1 4BS.
