---
pageTitle: Volunteer
---

Your people will volunteer freely in the day of Your power. Psalm 110:3

We are always in need of volunteers to get involved with serving at the till or helping with the bookshop.

Download the volunteer application form as a [pdf](/media/volunteer.pdf) or a [docx](/media/volunteer.docx).

Please give us your completed form in person at the till.
