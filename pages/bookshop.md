---
pageTitle: Bookshop
---
We sell both new books and second-hand and if we don’t have exactly what you want, just ask and we can almost certainly order it for you!

Caveat: This stocklist is still in progress and may not be entirely accurate. Apologies in advance if you can’t actually find in store what is on this list!

<h3 >Search Stocklist</h3>
<div id="booksBody">
<form>
<div class="form-group">
								<input id="isbn" class="form-control form-control-lg" type="text" placeholder="ISBN"  onChange="search('isbn')"><br />
								<input id="author" class="form-control" type="text" placeholder="Author" onChange="search('author')"><br />
								<input id="title" class="form-control" type="text" placeholder="Title" onChange="search('title')" ><br />
								<input id="publisher" class="form-control" type="text" placeholder="Publisher" onChange="search('publisher')">
							</div>
</p></form>
<p>						<button class="btn">Search</button></p>
<div id="searchResults"></div>
</p>
