---
pageTitle: Ministries
---

Our facilities are used by various other Christian ministries, including:

[Liverpool Homeless Outreach](http://www.liverpoolhomelessoutreach.co.uk/)

[Deeper Life Church](http://www.dclm-liverpool.org.uk/homeless.html)

[Healing Rooms](http://www.healingroomsliverpool.org.uk/index.html)

[City Hearts](http://city-hearts.co.uk/)

[Just Love](https://justloveliverpool.wordpress.com/)

[YWAM Liverpool](https://www.ywamliverpool.co.uk/)
